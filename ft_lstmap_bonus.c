/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap_bonus.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/15 14:07:49 by alouis            #+#    #+#             */
/*   Updated: 2019/11/15 14:07:52 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, void *(*f)(void *), void (*del)(void *))
{
	t_list *tmp;
	t_list *new;

	if (lst == 0 || f == 0)
		return (0);
	tmp = ft_lstnew((*f)(lst->content));
	new = tmp;
	while (lst->next != 0)
	{
		lst = lst->next;
		new->next = ft_lstnew((*f)(lst->content));
		if (!(new->next))
		{
			ft_lstclear(&tmp, del);
			return (0);
		}
		new = new->next;
	}
	new->next = 0;
	return (tmp);
}
