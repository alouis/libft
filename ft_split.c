/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/09 18:34:00 by alouis            #+#    #+#             */
/*   Updated: 2019/11/19 15:12:41 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	count_word(const char *s, char c)
{
	int	j;
	int	cw;

	j = 0;
	cw = 0;
	while (s[j])
	{
		while (s[j] == c)
		{
			j++;
			if (!(s[j]))
				return (cw);
		}
		cw++;
		while (s[j] != c && s[j])
			j++;
	}
	return (cw);
}

static char	*malloc_and_fill(const char *s, char c, int *i)
{
	size_t	len;
	int		ii;
	char	*word;

	len = 0;
	ii = *i;
	while (s[*i] != c && s[*i])
	{
		(*i)++;
		len++;
	}
	if (!(word = (char *)malloc(sizeof(char) * (len + 1))))
		return (NULL);
	len = 0;
	while (s[ii] != c && s[ii])
		word[len++] = s[ii++];
	word[len] = '\0';
	return (word);
}

char		**ft_split(char const *s, char c)
{
	int		i;
	int		k;
	char	**tab;

	i = 0;
	k = 0;
	if (!s)
		return (NULL);
	if (!(tab = (char **)malloc(sizeof(char *) * (count_word(s, c) + 1))))
		return (NULL);
	while (s[i])
	{
		if (s[i] != c && s[i])
		{
			tab[k++] = malloc_and_fill(s, c, &i);
		}
		while (s[i] == c && s[i])
			i++;
	}
	tab[k] = 0;
	return (tab);
}
