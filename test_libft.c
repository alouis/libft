#include "libft.h"

char	maj(unsigned int i, char c)
{
	(void) i;
	return (ft_toupper(c));
}

int	main(void)
{
//	printf("\nPART 1\n");
	
/*	const char *s1;
	const char *s2;
	const char *s3;
	const char *s4;
	printf("ft_strlen \n ***** \n");
	s1 = "";
	printf("s1 : %zu \n", strlen(s1));
	printf("s1 : %zu \n", ft_strlen(s1));
	s2 = "";
	printf("s2 : %zu \n", strlen(s2));
	printf("s2 : %zu \n", ft_strlen(s2));
	s3 = ".";
	printf("s3 : %zu \n", strlen(s3));
	printf("s3 : %zu \n", ft_strlen(s3));
	s4 = "0123456789abcdef";
	printf("s4 : %zu \n", strlen(s4));
	printf("s4 : %zu \n", ft_strlen(s4));
	printf("***** \n\n");

	int alpha;
	int notalpha;
	printf("ft_isalpha \n ***** \n");
	printf("alpha\n");
	alpha = 65;
	printf("%d\n", isalpha(alpha));
	printf("%d\n", ft_isalpha(alpha));
	alpha = 90;
	printf("%d\n", isalpha(alpha));
	printf("%d\n", ft_isalpha(alpha));
	alpha = 97;
	printf("%d\n", isalpha(alpha));
	printf("%d\n", ft_isalpha(alpha));
	alpha = 122;
	printf("%d\n", isalpha(alpha));
	printf("%d\n", ft_isalpha(alpha));
	printf("notalpha\n");
	notalpha = 64;
	printf("%d\n", isalpha(notalpha));
	printf("%d\n", ft_isalpha(notalpha));
	notalpha = 91;
	printf("%d\n", isalpha(notalpha));
	printf("%d\n", ft_isalpha(notalpha));
	notalpha = 96;
	printf("%d\n", isalpha(notalpha));
	printf("%d\n", ft_isalpha(notalpha));
	notalpha = 123;
	printf("%d\n", isalpha(notalpha));
	printf("%d\n", ft_isalpha(notalpha));
	printf("***** \n\n");*/

/*	int digit;
	int notdigit;
	printf("ft_isdigit \n ***** \n");
	printf("digit\n");
	digit = 48;
	printf("%d\n", isdigit(digit));
	printf("%d\n", ft_isdigit(digit));
	digit = 57;
	printf("%d\n", isdigit(digit));
	printf("%d\n", ft_isdigit(digit));
	digit = '5';
	printf("%d\n", isdigit(digit));
	printf("%d\n", ft_isdigit(digit));
	printf("notdigit\n");
	notdigit = 47;
	printf("%d\n", isdigit(notdigit));
	printf("%d\n", ft_isdigit(notdigit));
	notdigit = 58;
	printf("%d\n", isdigit(notdigit));
	printf("%d\n", ft_isdigit(notdigit));
	notdigit = 32;
	printf("%d\n", isdigit(notdigit));
	printf("%d\n", ft_isdigit(notdigit));
	notdigit = 0;
	printf("%d\n", isdigit(notdigit));
	printf("%d\n", ft_isdigit(notdigit));
	printf("***** \n\n");*/

/*	int alnum;
	int notalnum;

	printf("ft_isalnum \n ***** \n");
	printf("alnum\n");
	alnum = 48;
	printf("%d\n", isalnum(alnum));
	printf("%d\n", ft_isalnum(alnum));
	alnum = 'z';
	printf("%d\n", isalnum(alnum));
	printf("%d\n", ft_isalnum(alnum));
	alnum = 'Z';
	printf("%d\n", isalnum(alnum));
	printf("%d\n", ft_isalnum(alnum));
	printf("notalnum\n");
	notalnum = 127;
	printf("%d\n", isalnum(notalnum));
	printf("%d\n", ft_isalnum(notalnum));
	notalnum = 47;
	printf("%d\n", isalnum(notalnum));
	printf("%d\n", ft_isalnum(notalnum));
	notalnum = 0;
	printf("%d\n", isalnum(notalnum));
	printf("%d\n", ft_isalnum(notalnum));*/

/*	int ascii;
	int notascii;

	printf("ft_isascii \n ***** \n");
	printf("ascii\n");
	ascii = 127;
	printf("%d\n", isascii(ascii));
	printf("%d\n", ft_isascii(ascii));	
	printf("notascii\n");
	notascii = 128;
	printf("%d\n", isascii(notascii));
	printf("%d\n", ft_isascii(notascii));*/

/*	int prt;
	int notprt;

	printf("ft_isprint \n ***** \n");
	printf("prt\n");
	prt = 126;
	printf("%d\n", isprint(prt));
	printf("%d\n", ft_isprint(prt));	
	printf("notprt\n");
	notprt = 31;
	printf("%d\n", isprint(notprt));
	printf("%d\n", ft_isprint(notprt));*/

/*	int up;

	printf("ft_toupper \n ***** \n");
	up = 65;
	printf("%d\n", toupper(up));
	printf("%d\n", ft_toupper(up));	
	up = 90;
	printf("%d\n", toupper(up));
	printf("%d\n", ft_toupper(up));	
	up = 48;
	printf("%d\n", toupper(up));
	printf("%d\n", ft_toupper(up));
	up = 97;
	printf("%d\n", toupper(up));
	printf("%d\n", ft_toupper(up));
	up = 122;
	printf("%d\n", toupper(up));
	printf("%d\n", ft_toupper(up));*/

/*	int up;

	printf("ft_tolower \n ***** \n");
	up = 'A';
	printf("%d\n", tolower(up));
	printf("%d\n", ft_tolower(up));	
	up = 90;
	printf("%d\n", tolower(up));
	printf("%d\n", ft_tolower(up));	
	up = 48;
	printf("%d\n", tolower(up));
	printf("%d\n", ft_tolower(up));
	up = 97;
	printf("%d\n", tolower(up));
	printf("%d\n", ft_tolower(up));
	up = 122;
	printf("%d\n", tolower(up));
	printf("%d\n", ft_tolower(up));*/

/*	const char *s;
	int c;

	printf("ft_strchr \n ***** \n");
	s = "four";
	c = 'g';
	printf("%s\n", strchr(s, c));
	printf("%s\n", ft_strchr(s, c));
	s = "four";
	c = 'f';
	printf("%s\n", strchr(s, c));
	printf("%s\n", ft_strchr(s, c));
	s = "four";
	c = 'u';
	printf("%s\n", strchr(s, c));
	printf("%s\n", ft_strchr(s, c));
	s = "four";
	c = '\0';
	printf("%s\n", strchr(s, c));
	printf("%s\n", ft_strchr(s, c));
	s = "";
	c = '\0';
	printf("%s\n", strchr(s, c));
	printf("%s\n", ft_strchr(s, c));*/
	
/*	const char *s;
	int c;

	printf("ft_strrchr \n ***** \n");
	s = "parallel";
	c = 'p';
	printf("%s\n", strrchr(s, c));
	printf("%s\n", ft_strrchr(s, c));
	s = "parallel";
	c = 'e';
	printf("%s\n", strrchr(s, c));
	printf("%s\n", ft_strrchr(s, c));
	s = "parallel";
	c = '\0';
	printf("%s\n", strrchr(s, c));
	printf("%s\n", ft_strrchr(s, c));
	s = "\0";
	c = '\0';
	printf("%s\n", strrchr(s, c));
	printf("%s\n", ft_strrchr(s, c));
	s = "parallel";
	c = 'a';
	printf("%s\n", strrchr(s, c));
	printf("%s\n", ft_strrchr(s, c));
	s = "parallel";
	c = 'l';
	printf("%s\n", strrchr(s, c));
	printf("%s\n", ft_strrchr(s, c));*/
	

	
/*	const char *s1;
	const char *s2;
	size_t n;
		
	printf("ft_strncmp \n ***** \n");
	s1 = "0";
	s2 = "1";
	n = 1;
	printf("%d\n", strncmp(s1, s2, n));
	printf("%d\n", ft_strncmp(s1, s2, n));
	s1 = "hello";
	s2 = "hel";
	n = 4;
	printf("%d\n", strncmp(s1, s2, n));
	printf("%d\n", ft_strncmp(s1, s2, n));
	s1 = "hel";
	s2 = "hello";
	n = 4;
	printf("%d\n", strncmp(s1, s2, n));
	printf("%d\n", ft_strncmp(s1, s2, n));
	s1 = "1";
	s2 = "1";
	n = 0;
	printf("%d\n", strncmp(s1, s2, n));
	printf("%d\n", ft_strncmp(s1, s2, n));
	s1 = "1";
	s2 = "1";
	n = 1;
	printf("%d\n", strncmp(s1, s2, n));
	printf("%d\n", ft_strncmp(s1, s2, n));
	s1 = "\0";
	s2 = "1";
	n = 1;
	printf("%d\n", strncmp(s1, s2, n));
	printf("%d\n", ft_strncmp(s1, s2, n));
	s1 = "hello%";
	s2 = "hello&";
	n = 1;
	printf("%d\n", strncmp(s1, s2, n));
	printf("%d\n", ft_strncmp(s1, s2, n));*/


/*	char dst[10] = "bonjour";
	char src[16] = "goodmorning";
	size_t dstsize;
		
	printf("ft_strlcpy \n ***** \n");
	dstsize = 0;
	printf("%lu\n", strlcpy(dst, src, dstsize));
	printf("%lu\n", ft_strlcpy(dst, src, dstsize));
	dstsize = 4;
	printf("%lu\n", strlcpy(dst, src, dstsize));
	printf("%lu\n", ft_strlcpy(dst, src, dstsize));
	dstsize = 1;
	printf("%lu\n", strlcpy(dst, src, dstsize));
	printf("%lu\n", ft_strlcpy(dst, src, dstsize));*/

	
/*	char dst[100] = "bon";;
	char src[100] = "bon";
	size_t dstsize;
		
	printf("ft_strlcat \n ***** \n");
	dstsize = 3;
	printf("%lu\n", strlcat(dst, src, dstsize));
	printf("%lu\n", ft_strlcat(dst, src, dstsize));
	dstsize = 2;
	printf("%lu\n", strlcat(dst, src, dstsize));
	printf("%lu\n", ft_strlcat(dst, src, dstsize));
	dstsize = 5;
	printf("%lu\n", strlcat(dst, src, dstsize));
	printf("%lu\n", ft_strlcat(dst, src, dstsize));
	dstsize = -1;
#	printf("%lu\n", strlcat(dst, src, dstsize));
	printf("%lu\n", ft_strlcat(dst, src, dstsize));
	dstsize = 0;
	printf("%lu\n", strlcat(dst, src, dstsize));
	printf("%lu\n", ft_strlcat(dst, src, dstsize));*/

	
/*	char haystack1[50] = "haystack";
	char haystack2[50] = "hayneedlestack";
	char haystack3[50] = "hayneestaneedleck";
	char haystack4[50] = "";
	char needle1[6] = "needle";
	char needle2[6] = "";
	int len;
	
	printf("ft_strnstr \n ***** \n");
	len = 20;
	printf("%s\n", strnstr(haystack1, needle1, len));
	printf("%s\n", ft_strnstr(haystack1, needle1, len));
	printf("%s\n", strnstr(haystack2, needle1, len));
	printf("%s\n", ft_strnstr(haystack2, needle1, len));
	printf("%s\n", strnstr(haystack3, needle1, len));
	printf("%s\n", ft_strnstr(haystack3, needle1, len));
	printf("%s\n", strnstr(haystack4, needle1, len));
	printf("%s\n", ft_strnstr(haystack4, needle1, len));
	printf("%s\n", strnstr(haystack1, needle2, len));
	printf("%s\n\n", ft_strnstr(haystack1, needle2, len));
	len = 0;
	printf("%s\n", strnstr(haystack1, needle1, len));
	printf("%s\n", ft_strnstr(haystack1, needle1, len));
	printf("%s\n", strnstr(haystack2, needle1, len));
	printf("%s\n", ft_strnstr(haystack2, needle1, len));
	printf("%s\n", strnstr(haystack3, needle1, len));
	printf("%s\n", ft_strnstr(haystack3, needle1, len));
	printf("%s\n", strnstr(haystack4, needle1, len));
	printf("%s\n", ft_strnstr(haystack4, needle1, len));
	printf("%s\n", strnstr(haystack1, needle2, len));
	printf("%s\n", ft_strnstr(haystack1, needle2, len));*/

/*	char str1[10] = "str";
	char str2[10] = " \t -564kl";
	char str3[10] = "\v\n \f+lk";
	char str4[10] = " \r-3RE*(45";
	char str5[10] = "10";
	char str6[10] = "  ++45";
	
	printf("ft_atoi \n ***** \n");
	printf("%d\n", atoi(str1));
	printf("%d\n", ft_atoi(str1));
	printf("%d\n", atoi(str2));
	printf("%d\n", ft_atoi(str2));
	printf("%d\n", atoi(str3));
	printf("%d\n", ft_atoi(str3));
	printf("%d\n", atoi(str4));
	printf("%d\n", ft_atoi(str4));
	printf("%d\n", atoi(str5));
	printf("%d\n", ft_atoi(str5));
	printf("%d\n", atoi(str6));
	printf("%d\n", ft_atoi(str6));*/

/*	void *b;
	void *me;
	int c;
	size_t len;

	printf("ft_memset \n ***** \n");
	len = 5;
	c = '0';
	b = (void *)malloc(sizeof(char) * len);	
	printf("%s\n", memset(b, c, len));
	me = (void *)malloc(sizeof(char) * len);
	printf("%s\n\n", ft_memset(me, c, len));
	free(b);
	free(me);

	len = 50;
	c = 'o';
	b = (void *)malloc(sizeof(char) * len);	
	printf("%s\n", memset(b, c, len));
	me = (void *)malloc(sizeof(char) * len);
	printf("%s\n\n", ft_memset(me, c, len));
	free(b);
	free(me);

	len = 0;
	c = 'a';
	b = (void *)malloc(sizeof(char) * len);	
	printf("%s\n", memset(b, c, len));
	me = (void *)malloc(sizeof(char) * len);
	printf("%s\n\n", ft_memset(me, c, len));
	free(b);
	free(me);

	len = 5;
	c = 130;
	b = (void *)malloc(sizeof(char) * 1);	
	printf("%s\n", memset(b, c, len));
	me = (void *)malloc(sizeof(char) * 1);
	printf("%s\n\n", ft_memset(me, c, len));
	free(b);
	free(me);*/

/*	char dst[3];
	char src[10] = "helloyou";
	size_t n;

	printf("ft_memcpy \n ***** \n");
	n = 1;
	printf("b : ");
	printf("%s\n", memcpy(dst, src, n));
	printf("me : ");
	printf("%s\n", ft_memcpy(dst, src, n));

	n = 0;
	printf("b : ");
	printf("%s\n", memcpy(dst, src, n));
	printf("me : ");
	printf("%s\n", ft_memcpy(dst, src, n));

	n = 3;
	printf("b : ");
	printf("%s\n", memcpy(dst, src, n));
	printf("me : ");
	printf("%s\n", ft_memcpy(dst, src, n));

	printf("\nn > dst\n");
	n = 7;
	printf("b : ");
	printf("%s\n", memcpy(dst, src, n));
	printf("me :");
	printf("%s\n", ft_memcpy(dst, src, n));*/

/*	unsigned char dst[20] = "niagaolleh";
	unsigned char src[20] = "helloagain";
	int c;
	size_t n;

	printf("ft_memccpy \n ***** \n");
	printf("\nc = h\n");
	n = 5;
	c = 'h';
	printf("b : ");
	printf("%s\n", memccpy(dst, src, c, n));
	printf("me : ");
	printf("%s\n", ft_memccpy(dst, src, c, n));
	printf("\nc = a\n");
	n = 10;
	c = 'a';
	printf("b : ");
	printf("%s\n", memccpy(dst, src, c, n));
	printf("me : ");
	printf("%s\n", ft_memccpy(dst, src, c, n));
	printf("\nc = n\n");
	n = 10;
	c = 'n';
	printf("b : ");
	printf("%s\n", memccpy(dst, src, c, n));
	printf("me : ");
	printf("%s\n", ft_memccpy(dst, src, c, n));
	n = 1;
	c = 'n';
	printf("b : ");
	printf("%s\n", memccpy(dst, src, c, n));
	printf("me : ");
	printf("%s\n", ft_memccpy(dst, src, c, n));
	n = 0;
	c = 'n';
	printf("b : ");
	printf("%s\n", memccpy(dst, src, c, n));
	printf("me : ");
	printf("%s\n", ft_memccpy(dst, src, c, n));*/
	
/*	char dst[50] = "niagaolleh";
	char src[50] = "helloagain";
	size_t len;

	printf("ft_memmove \n ***** \n");
	len = 10;
	printf("b : ");
	printf("%s\n", memmove(dst, src, len));
	printf("me : ");
	printf("%s\n", ft_memmove(dst, src, len));

	len = 5;
	printf("b : ");
	printf("%s\n", memmove(dst, src, len));
	printf("me : ");
	printf("%s\n", ft_memmove(dst, src, len));
	
	len = 1;
	printf("b : ");
	printf("%s\n", memmove(dst, src, len));
	printf("me : ");
	printf("%s\n", ft_memmove(dst, src, len));

	len = 0;
	printf("b : ");
	printf("%s\n", memmove(dst, src, len));
	printf("me : ");
	printf("%s\n", ft_memmove(dst, src, len));*/

/*	char *s;
	char *me;
	int c;
	size_t n;

	printf("ft_memchr \n ***** \n");
	c = 'y';
	n = 6; 
	s = (char *)malloc(sizeof(char *) * n);
	me = (char *)malloc(sizeof(char *) * n);
	strcpy(me, "wheresyesterday");
	strcpy(s, "wheresyesterday");
	printf("\ns : ");
	printf("%s\n", memchr(s, c, n));
	printf("me : ");
	printf("%s\n", ft_memchr(s, c, n));	
	free(s);
	free(me);
	
	c = 'y';
	n = 8; 
	s = (char *)malloc(sizeof(char *) * n);
	me = (char *)malloc(sizeof(char *) * n);
	strcpy(me, "wheresyesterday");
	strcpy(s, "wheresyesterday");
	printf("\ns : ");
	printf("%s\n", memchr(s, c, n));
	printf("me : ");
	printf("%s\n", ft_memchr(s, c, n));	
	free(s);
	free(me);
	
	c = 'j';
	n = 18; 
	s = (char *)malloc(sizeof(char *) * n);
	me = (char *)malloc(sizeof(char *) * n);
	strcpy(me, "wheresyesterday");
	strcpy(s, "wheresyesterday");
	printf("\ns : ");
	printf("%s\n", memchr(s, c, n));
	printf("me : ");
	printf("%s\n", ft_memchr(s, c, n));	
	free(s);
	free(me);
	
	c = 'y';
	n = 0; 
	s = (char *)malloc(sizeof(char *) * n);
	me = (char *)malloc(sizeof(char *) * n);
	strcpy(me, "wheresyesterday");
	strcpy(s, "wheresyesterday");
	printf("\ns : ");
	printf("%s\n", memchr(s, c, n));
	printf("me : ");
	printf("%s\n", ft_memchr(s, c, n));	
	free(s);
	free(me);*/

/*	char *s1;
	char *s2;
	char *me1;
	char *me2;
	size_t n;

	printf("ft_memchr \n ***** \n");
	n = 10; 
	s1 = (char *)malloc(sizeof(char *) * n);
	s2 = (char *)malloc(sizeof(char *) * n);
	strcpy(s1, "44105");
	strcpy(s2, "44");
	printf("\ns : ");
	printf("%d\n", memcmp(s1, s2, n));
	me1 = (char *)malloc(sizeof(char *) * n);
	me2 = (char *)malloc(sizeof(char *) * n);
	strcpy(me1, "44105");
	strcpy(me2, "44");
	printf("me : ");
	printf("%d\n", ft_memcmp(s1, s2, n));	
	free(s1);
	free(s2);
	free(me1);
	free(me2);

	n = 2; 
	s1 = (char *)malloc(sizeof(char *) * n);
	s2 = (char *)malloc(sizeof(char *) * n);
	strcpy(s1, "44105");
	strcpy(s2, "44");
	printf("\ns : ");
	printf("%d\n", memcmp(s1, s2, n));
	me1 = (char *)malloc(sizeof(char *) * n);
	me2 = (char *)malloc(sizeof(char *) * n);
	strcpy(me1, "44105");
	strcpy(me2, "44");
	printf("me : ");
	printf("%d\n", ft_memcmp(s1, s2, n));	
	free(s1);
	free(s2);
	free(me1);
	free(me2);

	n = 5; 
	s1 = (char *)malloc(sizeof(char *) * n);
	s2 = (char *)malloc(sizeof(char *) * n);
	strcpy(s1, "441");
	strcpy(s2, "44102");
	printf("\ns : ");
	printf("%d\n", memcmp(s1, s2, n));
	me1 = (char *)malloc(sizeof(char *) * n);
	me2 = (char *)malloc(sizeof(char *) * n);
	strcpy(me1, "441");
	strcpy(me2, "44102");
	printf("me : ");
	printf("%d\n", ft_memcmp(s1, s2, n));	
	free(s1);
	free(s2);
	free(me1);
	free(me2);*/

/*	int nbr;
	int size;
	int *a;
	int *b;
//	char *aa;

	nbr = 16;
	size = 16;
	printf("ft_calloc \n ***** \n");
	printf("\nft_calloc : ");
	b = ft_calloc(nbr, size);
	while (nbr > 0)
	{
		printf("%d", *b++);
		nbr--;
	}
	printf("\ncalloc : ");
	a = calloc(nbr, size);
	while (nbr > 0)
	{
		printf("%d\n", *a++);
		nbr--;
	}*/

/*	char s1[10] = "bigorneau";
	char s2[10] = "bigorneau";
	printf("ft_strdup \n ***** \n");
	printf("\nstrdup : ");
	printf("%s\n", strdup(s2));
	printf("\nft_strdup : ");
	printf("%s\n", ft_strdup(s1));*/
	
	printf("\nPART 2\n");

/*	char s[17] = "0123456789abcdef";
	int start = 12;
	int len = 10;
	int len2 = 0;
	int len3 = 20;
	char *s1;

	printf("\nft_substr\n *****\n");

	printf("s < len\n");
	s1 = ft_substr(s, start, len);
	if (s1 != NULL)
		printf("OK\n");
	printf("%s\n\n", s1);
	free(s1);

	printf("s = len\n");
	s1 = ft_substr(s, start, len2);
	printf("%s\n\n", s1);
	free(s1);
	
	printf("s > len\n");
	s1 = ft_substr(s, start, len3);
	printf("%s\n\n", s1);
	free(s1);*/

/*	char s0[] = "";
	char s1[] = "0123456789";
	char s2[] = "abcdef";
	char *dst;
	
	printf("\nft_strjoin\n *****\n");
	
	dst = ft_strjoin(s0, s1);
	printf("NULL : %s\n", dst);
	free(dst);

	dst = ft_strjoin(s2, s0);
	printf("NULL : %s\n", dst);
	free(dst);

	dst = ft_strjoin(s1, s2);
	printf("join : %s\n", dst);
	free(dst);

	dst = ft_strjoin(s2, s1);
	printf("join : %s\n", dst);
	free(dst);

	dst = ft_strjoin(0, 0);
//	printf("join : %s\n", dst);
	free(dst);*/
	

	
/*	char s0[] = "sep456set242pessp";
	char s1[] = "sep456set242pe";
	char s2[] = "455pes556";
	char s3[] = "p654";
	char s4[] = "786esfpe";
	char s5[] = "sepespspepe";
	char s6[] = "";
	char s7[] = "tsep";
	char sep[] = "sep";
	char sep1[] = "";
	char *result;

	printf("\nft_strtrim\n ***** \n");

	result = ft_strtrim(s0, sep);
	printf("s0 : %s\n", result);
	free(result);
	result = ft_strtrim(s1, sep);
	printf("s1 : %s\n", result);
	free(result);
	result = ft_strtrim(s2, sep);
	printf("s2 : %s\n", result);
	free(result);
	result = ft_strtrim(s3, sep);
	printf("s3 : %s\n", result);
	free(result);
	result = ft_strtrim(s4, sep);
	printf("s4 : %s\n", result);
	free(result);
	result = ft_strtrim(s5, sep);
	if (result != 0)
		printf("OK\n");
	printf("NULL : %s\n", result);
	free(result);
	result = ft_strtrim(s6, sep);
	printf("NULL : %s\n", result);
	free(result);
	result = ft_strtrim(s6, sep1);
	printf("s4 no set : %s\n", result);
	free(result);
	result = ft_strtrim(s7, sep);
	printf("s7 : %s\n", result);
	free(result);*/

	
/*	int n = -654;
	int fd = 1;

	printf("\nft_putnbr_fd\n *****\nn");
	printf("%d\n", ft_putnbr_fd(n, fd));*/


/*	int n0 = 10;
	int n1 = 0;
	int n2 = -1;
	int n3 = 2356;
	int n4 = -2356;
	int n5 = 9;
	int n6 = 5;
	int n7 = 1;

	char *c0;
	char *c1;
	char *c2;
	char *c3;
	char *c4;
	char *c5;
	char *c6;
	char *c7;

	printf("\nft_itoa\n *****\n");
	c0 = ft_itoa(n0);
	printf("n = 10 : %s\n", c0);
	free(c0);
	c1 = ft_itoa(n1);
	printf("n = 0 : %s\n", c1);
	free(c1);
	c2 = ft_itoa(n2);
	printf("n = -1 : %s\n", c2);
	free(c2);
	c3 = ft_itoa(n3);
	printf("n = -1 : %s\n", c3);
	free(c3);
	c4 = ft_itoa(n4);
	printf("n = -1 : %s\n", c4);
	free(c4);
	c5 = ft_itoa(n5);
	printf("n = -1 : %s\n", c5);
	free(c5);
	c6 = ft_itoa(n6);
	printf("n = -1 : %s\n", c6);
	free(c6);
	c7 = ft_itoa(n7);
	printf("n = -1 : %s\n", c7);
	free(c7);*/

/*	char s[30] = "-split -fon--ctionn-e----.-";
	char **tab;
	int i;

	printf("\nft_split\n *****\n");
	
	i = 0;
	tab = ft_split(s, '-');
	while (tab[i])
	{
		printf("%s\n", tab[i]);
		i++;
	}
	free(tab);*/


//	char s[] =  ""; //" 42 : best way to.. loose your weekends (; !!";
	char *res;

	printf("\nft_strmapi\n *****\n");

	res = ft_strmapi(0, &maj);
	printf("MAJ : %s\n", res);
	free(res);


	return(1);
}
