/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/09 10:33:01 by alouis            #+#    #+#             */
/*   Updated: 2019/11/16 14:14:52 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	char		*d;
	const char	*s;
	char		*d2;
	const char	*s2;

	d = dst;
	s = src;
	if (d == 0 && s == 0)
		return (dst);
	if (d < s)
	{
		while (len--)
			*d++ = *s++;
	}
	else
	{
		d2 = d + (len - 1);
		s2 = s + (len - 1);
		while (len--)
			*d2-- = *s2--;
	}
	return (dst);
}
